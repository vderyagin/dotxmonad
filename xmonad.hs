import qualified Bindings
import qualified Colors
import qualified Dzen
import qualified Layouts
import qualified ManageHook
import qualified MiscOptions
import qualified Workspaces
import qualified XMonad
import qualified SystemState

main :: IO ()
main = do
  sysState <- SystemState.getSystemState
  XMonad.def
    >>= Dzen.configure sysState
    <$> ( Bindings.configure sysState
        . Layouts.configure sysState
        . Colors.configure
        . ManageHook.configure
        . Workspaces.configure
        . MiscOptions.configure
        )
    >>= XMonad.xmonad

-- Local Variables:
-- flycheck-ghc-search-path: ("lib")
-- End:
