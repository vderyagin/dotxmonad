module ShellPrompt ( promptConfig
                   , shellPrompt
                   , shellPromptTerminal) where

import qualified Colors
import qualified Fonts
import           XMonad
import           XMonad.Actions.SpawnOn
import           XMonad.Prompt
import           XMonad.Prompt.AppLauncher as AL

promptConfig :: XPConfig
promptConfig =
  def { font              = Fonts.prompt
      , bgColor           = Colors.foregroundDark
      , fgColor           = Colors.foreground
      , fgHLight          = Colors.foregroundDark
      , bgHLight          = Colors.foreground
      , promptBorderWidth = 0
      , position          = Bottom
      , height            = 30
      , historySize       = 50
      , historyFilter     = deleteAllDuplicates
      }

shellPrompt :: X ()
shellPrompt = shellPromptHere promptConfig

shellPromptTerminal :: String -> X ()
shellPromptTerminal = AL.launchApp promptConfig . (++ " -e")
