{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE ScopedTypeVariables #-}

module ManageHook (configure) where

import           Data.Monoid                (Endo)
import qualified NamedScratchpads
import           Queries                    (isMenu, isGame, isAnyOf, isVideoPlayer, isBrowser, isTempEmacsWindow)
import           XMonad
import qualified Workspaces
import           XMonad.Actions.SpawnOn     (manageSpawn)
import           XMonad.Hooks.EwmhDesktops  (ewmhFullscreen, ewmh)
import           XMonad.Hooks.ManageDocks   (docks)
import           XMonad.Hooks.ManageHelpers (doCenterFloat, doFullFloat, isDialog, isFullscreen)
import           XMonad.Hooks.SetWMName     (setWMName)
import           XMonad.Hooks.UrgencyHook   (NoUrgencyHook (..),
                                             withUrgencyHook)

shiftTo :: Int -> ManageHook
shiftTo = doShift . Workspaces.workspace

appsHook :: Query (Endo WindowSet)
appsHook = mconcat . concatMap (\(hook, queries) -> map (--> hook) queries) $ applicationRules

applicationRules :: [(ManageHook, [Query Bool])]
applicationRules =
  [ (doFloat,       [isMenu, className `isAnyOf` ["Xmessage", "Skype"]])
  , (doCenterFloat, [isTempEmacsWindow, isDialog, isVideoPlayer, className `isAnyOf` ["Animate", "Pinentry", "pinentry", "Gcr-prompter", "Peek"]])
  , (doIgnore,      [resource `isAnyOf` ["stalonetray", "desktop_window"]])
  , (doFullFloat,   [isFullscreen, isGame])
  , (shiftTo 1,     [isBrowser])
  , (shiftTo 5,     [className =? "Slack"])
  , (shiftTo 9,     [className =? "Zeal"])
  , (shiftTo 10,    [className =? "qBittorrent" <&&> not <$> isDialog])
  ]

configure :: forall (l :: * -> *). LayoutClass l Window => XConfig l -> XConfig l
configure c = uHook $ c { manageHook  = mHook
                        , startupHook = setWMName "LG3D"
                        }
  where mHook :: Query (Endo WindowSet)
        mHook = mconcat [ appsHook
                        , manageSpawn
                        , NamedScratchpads.hook
                        ]
        uHook :: forall (k :: * -> *). LayoutClass k Window => XConfig k -> XConfig k
        uHook = ewmhFullscreen . ewmh . docks . withUrgencyHook NoUrgencyHook
