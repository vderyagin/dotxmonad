module Fonts ( dzenMono
             , dzen
             , tabs
             , prompt) where

dzenMono :: String
dzenMono = "DejaVu Sans Mono-10"
dzen     :: String
dzen     = "DejaVu Sans-10"
tabs     :: String
tabs     = "xft:DejaVu Sans:pixelsize=24"
prompt   :: String
prompt   = "xft:Consolas:pixelsize=24"
