{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Dzen (configure) where

import qualified Colors
import           SystemState
import           Data.List                   (intercalate)
import qualified Fonts
import           System.FilePath
import           System.IO
import           XMonad
import           XMonad.Hooks.DynamicLog
import           XMonad.Util.NamedScratchpad
import           XMonad.Util.Run             (spawnPipe)
import           XMonad.Util.WorkspaceCompare (filterOutWs)

data Alignment = L | C | R

instance Show Alignment where
  show L = "l"
  show C = "c"
  show R = "r"

data DzenConfig = DzenConfig { alignment  :: Alignment
                             , background :: String
                             , foreground :: String
                             , font       :: String
                             , coordX     :: Int
                             , coordY     :: Int
                             , width      :: Int
                             , height     :: Int
                             , actions    :: [String]
                             , command    :: Maybe String
                             }

instance Default DzenConfig where
  def = DzenConfig { alignment   = L
                   , background  = Colors.background
                   , foreground  = Colors.foreground
                   , font        = Fonts.dzen
                   , coordX      = 0
                   , coordY      = 0
                   , width       = undefined
                   , height      = 32
                   , actions     = ["onstart=lower"]
                   , command     = Nothing
                   }

dzenConfigCmd :: DzenConfig -> String
dzenConfigCmd DzenConfig {..} =
  unwords $ case command of
              Just cmd -> [cmd, "|", "dzen2"] ++ args
              Nothing  -> "dzen2" : args
  where wrapInQuotes = ("'" ++) . (++ "'")
        args = [ "-x", show coordX
               , "-y", show coordY
               , "-w", show width
               , "-h", show height
               , "-bg", wrapInQuotes background
               , "-fg", wrapInQuotes foreground
               , "-fn", wrapInQuotes font
               , "-ta", show alignment
               , "-dock"
               , "-e", wrapInQuotes . intercalate ";" $ actions]


pp :: Handle -> FilePath -> PP
pp handle homeDir =
  def { ppCurrent         = dzenColor Colors.foreground Colors.foregroundDark . wrap " " " "
      , ppHidden          = dzenColor Colors.foreground ""
      , ppHiddenNoWindows = dzenColor Colors.foregroundDark ""
      , ppUrgent          = dzenColor Colors.background Colors.urgent . wrap " " " "
      , ppSep             = " "
      , ppSort            = (. filterOutWs [scratchpadWorkspaceTag]) <$> ppSort def
      , ppOrder           = \(ws:l:_) -> [ws,l]
      , ppOutput          = hPutStrLn handle
      , ppLayout          = layoutIcon homeDir
      }

layoutIcon :: FilePath -> String -> String
layoutIcon homeDir layoutName
  | layout `elem` layouts = icon
  | otherwise = noIcon
    where
      layout       = last . words $ layoutName
      layouts      = ["vertical", "horizontal", "tabs", "full", "Full", "grid"]
      icon         = dzenColor Colors.foreground Colors.background $ "^i(" ++ iconLocation ++ ")"
      noIcon       = dzenColor Colors.urgent Colors.background "?"
      iconLocation = homeDir </> ".icons" </> "dzen2" </> layout <.> "xbm"

type HostName = String

statusOffset :: HostName -> Int
statusOffset "t480s" = 700
statusOffset _ = 350

hres :: SystemState -> Int
hres ss =
  case horizontalResolution ss of
    1920 -> 3264
    other -> other

runClock :: SystemState -> IO Handle
runClock sysState = spawnPipe . dzenConfigCmd $ dzConf
  where dzConf = def { coordX    = statusOffset (hostName sysState)
                     , width     = hres sysState - statusOffset (hostName sysState)
                     , alignment = R
                     , command   = Just "while :; do date +'%A, %d - %T ' || exit 1; sleep 1; done"
                     }

runStatus :: HostName -> IO Handle
runStatus h = spawnPipe . dzenConfigCmd $ dzConf
  where dzConf = def { width = statusOffset h
                     , font  = Fonts.dzenMono
                     , height = dzenHeight h
                     }

dzenHeight :: HostName -> Int
dzenHeight "t480s" = 32
dzenHeight _ = 16

configure :: forall (l :: * -> *). SystemState -> XConfig l -> IO (XConfig l)
configure sysState conf = do
  status <- runStatus $ hostName sysState
  _      <- runClock sysState

  return $ conf {logHook = dynamicLogWithPP $ pp status (homeDir sysState)}
