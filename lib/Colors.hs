{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Colors ( foreground
              , foregroundDark
              , background
              , urgent
              , configure) where

import           XMonad

foreground     :: String
foreground     = "#333333"
foregroundDark :: String
foregroundDark = "#d0d0d0"
background     :: String
background     = "#e5e5e5"
urgent         :: String
urgent         = "#bd4747"

configure :: forall (l :: * -> *). XConfig l -> XConfig l
configure c = c { focusedBorderColor = urgent
                , normalBorderColor  = foregroundDark
                }
