module Queries ( isAnyOf
               , isMenu
               , isGame
               , isBrowser
               , isVideoPlayer
               , isTempEmacsWindow
               , isImageViewer) where

import           Data.Monoid                (Any (..))
import           Data.List                  (isPrefixOf)
import           XMonad
import           XMonad.Hooks.ManageHelpers (isInProperty)

isAnyOf :: Query String -> [String] -> Query Bool
isAnyOf q = fmap getAny . mconcat . map (fmap Any . (q =?))

isMenu :: Query Bool
isMenu = isInProperty "_NET_WM_WINDOW_TYPE" "_NET_WM_WINDOW_TYPE_MENU"

isGame :: Query Bool
isGame = title `isAnyOf` gameTitles <||> className `isAnyOf` gameClasses
  where gameTitles  = ["Besiege", "Big Pharma", "RimWorld", "Talos - Linux", "MiniMetro"]
        gameClasses = ["factorio", "Dwarf_Fortress", "PapersPlease"]

isBrowser :: Query Bool
isBrowser = (className =? "Chromium-browser-chromium" <&&> resource =? "chromium-browser-chromium") <||>
             className =? "Google-chrome-unstable" <||>
            (className =? "Navigator" <&&> resource =? "Firefox") <||>
            (className =? "Brave-browser" <&&> resource =? "brave-browser")

isVideoPlayer :: Query Bool
isVideoPlayer = className `isAnyOf` ["mpv", "vlc", "Vlc", "ffplay", "Kodi"]

isImageViewer :: Query Bool
isImageViewer = className `isAnyOf` ["feh", "Animate"]

isTempEmacsWindow :: Query Bool
isTempEmacsWindow = className =? "Emacs" <&&> ("Edit with Emacs" `isPrefixOf`) <$> title
