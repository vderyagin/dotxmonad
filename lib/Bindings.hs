{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Bindings (configure) where

import qualified Colors
import           Data.Map                            (Map, fromList)
import           Layouts                             (layoutPrompt)
import qualified NamedScratchpads
import           ShellPrompt                         (shellPrompt,
                                                      shellPromptTerminal)
import           System.Exit                         (exitSuccess)
import           SystemState
import qualified Workspaces
import qualified Actions
import           XMonad
import qualified XMonad.Actions.ConstrainedResize    as Sqr
import           XMonad.Actions.CycleRecentWS
import           XMonad.Actions.CycleSelectedLayouts (cycleThroughLayouts)
import qualified XMonad.Actions.FlexibleResize       as Flex
import           XMonad.Actions.FloatKeys            (keysMoveWindow,
                                                      keysResizeWindow)
import           XMonad.Actions.GridSelect           (GSConfig (..),
                                                      goToSelected)
import           XMonad.Actions.NoBorders            (toggleBorder)
import           XMonad.Actions.PerWorkspaceKeys     (bindOn)
import           XMonad.Actions.SinkAll              (sinkAll)
import           XMonad.Actions.SpawnOn              (spawnHere)
import           XMonad.Actions.Submap               (submap)
import           XMonad.Actions.Warp
import           XMonad.Actions.WindowGo             (raiseNextMaybe)
import           XMonad.Actions.WithAll              (killAll)
import           XMonad.Hooks.ManageDocks            (ToggleStruts (..))
import           XMonad.Hooks.UrgencyHook            (clearUrgents, focusUrgent)
import           XMonad.Layout.LayoutCombinators     (JumpToLayout (..))
import           XMonad.Layout.MultiToggle           (Toggle (..))
import           XMonad.Layout.MultiToggle.Instances (StdTransformers (..))
import           XMonad.Layout.Reflect               (REFLECTX (..),
                                                      REFLECTY (..))
import           XMonad.Util.EZConfig

import           XMonad.Layout.ResizableTile         (MirrorResize (..))
import qualified XMonad.StackSet                     as W

type HostName = String

keymap :: HostName -> XConfig Layout -> Map (KeyMask, KeySym) (X ())
keymap host conf = mkKeymap conf $ concat maps
  where maps = [ layoutModification
               , misc conf
               , multimedia host
               , namedScratchpads
               , windowMove
               , windowNavigation
               , windowResize
               ]

windowResize :: [(String, X ())]
windowResize =
  [ ("M-<Page_Up>",   withFocused (keysResizeWindow (18, 18) (0.5, 0.5)))
  , ("M-<Page_Down>", withFocused (keysResizeWindow (-18, -18) (0.5, 0.5)))
  , ("M-C-<L>",       withFocused (keysResizeWindow (-18, 0) (0.5, 0.5)))
  , ("M-C-<R>",       withFocused (keysResizeWindow (18, 0) (0.5, 0.5)))
  , ("M-C-<U>",       withFocused (keysResizeWindow (0, 18) (0.5, 0.5)))
  , ("M-C-<D>",       withFocused (keysResizeWindow (0, -18) (0.5, 0.5)))
  ]

windowMove :: [(String, X ())]
windowMove =
  [ ("M-M1-<L>", withFocused (keysMoveWindow (-10, 0)))
  , ("M-M1-<R>", withFocused (keysMoveWindow (10, 0)))
  , ("M-M1-<U>", withFocused (keysMoveWindow (0, -10)))
  , ("M-M1-<D>", withFocused (keysMoveWindow (0, 10)))
  ]

windowNavigation :: [(String, X ())]
windowNavigation =
  [ ("M-<Tab>",    windows W.focusDown)
  , ("M-j",        windows W.focusDown)
  , ("M-k",        windows W.focusUp)
  , ("M-S-<Tab>",  windows W.focusUp)
  , ("M-S-j",      windows W.swapDown)
  , ("M-S-k",      windows W.swapUp)
  , ("M-<Return>", windows W.swapMaster)
  ]

layoutModification :: [(String, X ())]
layoutModification =
  [ ("M-t",   withFocused $ windows . W.sink)
  , ("M-S-t", sinkAll)
  , ("M-,",   sendMessage (IncMasterN 1))
  , ("M-.",   sendMessage (IncMasterN (-1)))
  , ("M-b",   sendMessage ToggleStruts)
  , ("M-S-b", withFocused toggleBorder)
  , ("M-h",   sendMessage Shrink)
  , ("M-l",   sendMessage Expand)
  , ("M-S-l", sendMessage MirrorShrink)
  , ("M-S-h", sendMessage MirrorExpand)
  , ("M-f",   sendMessage . Toggle $ FULL)
  , ("M-<R>", sendMessage . Toggle $ REFLECTX)
  , ("M-<L>", sendMessage . Toggle $ REFLECTX)
  , ("M-<U>", sendMessage . Toggle $ REFLECTY)
  , ("M-<D>", sendMessage . Toggle $ REFLECTY)
  , ("M-r",   sendMessage . Toggle $ MIRROR)
  ]

mouse :: XConfig Layout -> Map (KeyMask, Button) (Window -> X ())
mouse XConfig {modMask} = fromList
  [ ((modMask, button1),               \w -> focus w >> mouseMoveWindow w)
  , ((modMask, button2),               \w -> focus w >> windows W.shiftMaster)
  , ((modMask, button3),               \w -> focus w >> Flex.mouseResizeWindow w)
  , ((modMask, button4),               const $ windows W.focusUp)
  , ((modMask, button5),               const $ windows W.focusDown)
  , ((modMask .|. shiftMask, button3), \w -> focus w >> Sqr.mouseResizeWindow w True)
  , ((modMask .|. shiftMask, button4), const $ windows W.swapUp)
  , ((modMask .|. shiftMask, button5), const $ windows W.swapDown)
  ]

multimedia :: HostName -> [(String, X ())]
multimedia "desktop" =
  [ ("<XF86AudioMute>",  spawn "amixer -q set Master toggle")
  , ("M-<XF86HomePage>", spawn "toggle_screen_orientation")
  , ("<XF86HomePage>",   spawn "(sleep 0.5; xset dpms force off;) & rake --system lock_screen")
  , ("C-<XF86HomePage>", spawn "sleep 0.5; xset dpms force off")
  , ("M-<F7>",           spawn "sleep 0.5; xset dpms force off")

  , ("<XF86Mail>",   spawn "mpc -q toggle")
  , ("M-<XF86Mail>", spawn "mpd_status")

  , ("<XF86AudioLowerVolume>", spawn "mpc -q volume -5")
  , ("<XF86AudioRaiseVolume>", spawn "mpc -q volume +5")

  , ("C-<XF86AudioLowerVolume>", spawn "mpc -q prev")
  , ("C-<XF86AudioRaiseVolume>", spawn "mpc -q next")

  , ("S-<XF86AudioLowerVolume>", spawn "mpc -q seek -00:00:10")
  , ("S-<XF86AudioRaiseVolume>", spawn "mpc -q seek +00:00:10")

  , ("M1-<XF86AudioLowerVolume>", spawn "mpc -q seek -10%")
  , ("M1-<XF86AudioRaiseVolume>", spawn "mpc -q seek +10%")
  ]

multimedia "t480s" =
  [ ("<XF86AudioMute>",         spawn "amixer -q -D pulse sset Master toggle")
  , ("<XF86AudioLowerVolume>",  spawn "mpc -q volume -5")
  , ("<XF86AudioRaiseVolume>",  spawn "mpc -q volume +5")
  , ("<XF86AudioMicMute>",      spawn "amixer -q -D pulse sset Capture toggle")
  , ("<XF86MonBrightnessDown>", spawn "xbacklight -dec 5")
  , ("<XF86MonBrightnessUp>",   spawn "xbacklight -inc 5")
  , ("<XF86Display>",           spawn "sleep 0.5; xset dpms force off")
  , ("S-<XF86Display>",         spawn "rake --system lock_screen & sleep 1 && sudo loginctl suspend")
  -- For use w/ WASD keyboard:
  , ("M-<F7>",                  spawn "sleep 0.5; xset dpms force off")
  , ("M-S-<F7>",                spawn "(sleep 0.5; xset dpms force off;) & rake --system lock_screen")
  , ("M-C-<F7>",                spawn "rake --system lock_screen & sleep 1 && sudo loginctl suspend")
  ]

multimedia host = error $ "unknown hostname: " ++ host

cycleRecentNonEmptyWS' :: [KeySym] -> KeySym -> KeySym -> X ()
cycleRecentNonEmptyWS' = cycleWindowSets $ map W.tag . filter p . W.hidden
  where
    p, notEmpty, notScratchpad :: WindowSpace -> Bool
    p = notEmpty <&&> notScratchpad
    notEmpty = not . null . W.stack
    notScratchpad = (/= "NSP") . W.tag

misc :: XConfig Layout -> [(String, X ())]
misc conf@XConfig{terminal, layoutHook, workspaces} =
  [ ("M-<Backspace>",   focusUrgent)
  , ("M-S-<Backspace>", clearUrgents)
  , ("M-n",             cycleRecentNonEmptyWS' [xK_Super_L] xK_n xK_m)
  , ("M-S-c",           kill)
  , ("M-C-c",           killAll)
  , ("M-<Esc>",         layoutPrompt)
  , ("M-<F1>",          sendMessage . JumpToLayout $ "vertical")
  , ("M-<F2>",          sendMessage . JumpToLayout $ "horizontal")
  , ("M-<F3>",          sendMessage . JumpToLayout $ "tabs")
  , ("M-<F4>",          sendMessage . JumpToLayout $ "full")
  , ("M-<F5>",          sendMessage . JumpToLayout $ "grid")

  , ("M-S-n", refresh)
  -- , ("M-S-m", windows W.focusMaster)
  , ("M-S-q", io exitSuccess)

  , ("M-a",   shellPrompt)
  , ("M-S-a", shellPromptTerminal terminal)

  , ("M-<Print>",   spawn "scrot")
  , ("M-S-<Print>", spawn "sleep 0.2 && scrot -s")
  , ("M-C-<Print>", spawn "rake --system share_screenshot")

  , ("M-d", spawnHere $ "dmenu_run -i -b -p 'Run:' -fn 'Consolas-12:normal' -l 10"
                    ++ " -nb '" ++ Colors.foregroundDark ++ "' -nf '" ++ Colors.foreground
                    ++ "' -sb '" ++ Colors.foreground ++ "' -sf '" ++ Colors.foregroundDark ++ "'")

  , ("M-g", goToSelected myGSConfig)

  , ("M-<Space>", bindOn [ (Workspaces.workspace 1, cycleThroughLayouts ["full", "tabs"])
                         , (Workspaces.workspace 2, cycleThroughLayouts ["full", "tabs"])
                         , (Workspaces.workspace 3, cycleThroughLayouts ["full", "tabs"])
                         , (Workspaces.workspace 4, cycleThroughLayouts ["full", "tabs"])
                         , (Workspaces.workspace 5, cycleThroughLayouts ["full", "tabs"])
                         , (Workspaces.workspace 6, cycleThroughLayouts ["full", "tabs"])
                         , ("", cycleThroughLayouts ["full", "horizontal", "vertical", "tabs", "grid"])
                         ])

  , ("M-q", spawn "xmonad --recompile && killall -9 dzen2; xmonad --restart")

  , ("M-S-<Space>",  setLayout layoutHook)
  , ("M-S-<Return>", spawn terminal)

  , ("M-c", sm [ ("k", sm [ ("c", spawn "killall -9 chrome brave")
                          , ("e", spawn "killall -9 emacs")
                          , ("f", spawn "killall -9 firefox")
                          , ("s", spawn "killall -9 slack")
                          , ("i", spawn "killall -9 feh animate mcomix")
                          , ("t", spawn "killall -9 qbittorrent")
                          , ("m", spawn "killall -9 mplayer mpv vlc")])
               , ("l", sm [ ("e", spawn "emxkb 0")          -- English
                          , ("u", spawn "emxkb 1")          -- Ukrainian
                          , ("r", spawn "emxkb 2")])])      -- Russian
  , ("M-x", sm [ ("c",     Actions.goToBrowser)
               , ("M-c",   windows (W.view . Workspaces.workspace $ 1) >> spawnHere "brave-bin")
               , ("M-C-c", windows (W.view . Workspaces.workspace $ 1) >> spawnHere "brave-bin --incognito")
               , ("M-S-c", windows (W.view . Workspaces.workspace $ 1) >> spawnHere "brave-bin --tor --incognito")

               , ("M-e", spawn "emacsclient -c -a ''")
               , ("e",   raiseNextMaybe (spawn "emacsclient -c -a ''") (className =? "Emacs"))

               , ("s", Actions.goToSlack)
               , ("t", raiseNextMaybe (windows (W.view . Workspaces.workspace $ 10) >> spawnHere "qbittorrent")
                                      (className =? "qBittorrent"))
               , ("z", raiseNextMaybe (windows (W.view . Workspaces.workspace $ 9) >> spawnHere "zeal")
                                      (className =? "Zeal"))
               ])
  ]
  ++
  [(m ++ i, windows $ f j)
   | (i, j) <- zip (map return "1234567890") workspaces
   , (m, f) <- [("M-", W.view), ("M-S-", W.shift)]]
  ++
  [(m ++ k, screenWorkspace i >>= flip whenJust (windows . f) >> warpToScreen i 0.5 0.5)
   | (i, k) <- [(0, "w"), (1, "e")]
   , (m, f) <- [("M-", W.view), ("M-S-", W.shift)]]
  where myGSConfig :: GSConfig Window
        myGSConfig = def { gs_cellheight = 40
                         , gs_cellwidth  = 150
                         }
        sm :: [(String, X ())] -> X ()
        sm = submap . mkKeymap conf

namedScratchpads :: [(String, X ())]
namedScratchpads =
  [ ("M-p",   NamedScratchpads.toggle "terminal")
  , ("M-S-p", NamedScratchpads.toggle "dev-terminal")
  , ("M-i",   NamedScratchpads.toggle "image-viewer")
  , ("M-m",   NamedScratchpads.toggle "video-player")
  ]

configure :: forall (l :: * -> *). SystemState -> XConfig l -> XConfig l
configure sysState conf =
  conf { keys          = keymap (hostName sysState)
       , modMask       = mod4Mask
       , mouseBindings = mouse
       }
