{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Workspaces ( workspaces
                  , workspace
                  , configure) where

import qualified XMonad

workspaces :: [String]
workspaces = map return "αβγδεζηθικ" ++ ["NSP"]

workspace :: Int -> String
workspace = (workspaces !!) . subtract 1

configure :: forall (l :: * -> *). XMonad.XConfig l -> XMonad.XConfig l
configure c = c {XMonad.workspaces = workspaces}
