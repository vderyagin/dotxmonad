module Layouts ( layoutPrompt
               , configure) where

import qualified SystemState
import qualified Colors
import qualified Fonts
import           ShellPrompt                         (promptConfig)
import qualified Workspaces
import           XMonad                              hiding ((|||))
import           XMonad.Hooks.ManageDocks
import           XMonad.Layout.Grid
import           XMonad.Layout.LayoutCombinators
import           XMonad.Layout.MultiToggle
import           XMonad.Layout.MultiToggle.Instances
import           XMonad.Layout.Named
import           XMonad.Layout.NoBorders
import           XMonad.Layout.PerWorkspace
import           XMonad.Layout.Reflect
import           XMonad.Layout.ResizableTile
import           XMonad.Layout.Tabbed
import           XMonad.Prompt
import           XMonad.Prompt.Input

type HostName = String

tabConfig :: HostName -> Theme
tabConfig hostName =
  def { inactiveColor       = Colors.background
      , activeColor         = Colors.foregroundDark
      , urgentColor         = Colors.urgent
      , inactiveBorderColor = Colors.foregroundDark
      , activeBorderColor   = Colors.foregroundDark
      , urgentBorderColor   = Colors.urgent
      , inactiveTextColor   = Colors.foreground
      , activeTextColor     = Colors.foreground
      , urgentTextColor     = Colors.background
      , decoHeight          = decorationHeight hostName
      , fontName            = Fonts.tabs
      }
  where decorationHeight "t480s" = 28
        decorationHeight _       = 17

layoutPrompt :: X ()
layoutPrompt = prompt ?+ sendMessage . JumpToLayout
  where prompt  = inputPromptWithCompl promptConfig "Layout" compl
        compl   = mkComplFunFromList' promptConfig layouts
        layouts = ["vertical", "horizontal", "full", "tabs", "grid"]

vertical = named "vertical" $ ResizableTall nmaster delta frac slaves
  where (nmaster, delta, frac, slaves) = (1, 1/100, 3/5, [1])

horizontal = named "horizontal" . Mirror $ ResizableTall nmaster delta frac slaves
  where (nmaster, delta, frac, slaves) = (1, 1/100, 1/2, [1])


hook hostName = avoidStruts .
                smartBorders .
                mkToggle modifiers .
                onWorkspaces (map Workspaces.workspace [1..4]) tabsFirstLayout $ defaultLayout
  where
    grid            = named "grid" Grid
    tabs            = named "tabs" . tabbed shrinkText . tabConfig $ hostName
    full            = named "full" Full
    modifiers       = REFLECTX ?? REFLECTY ?? MIRROR ?? FULL ?? EOT
    defaultLayout   = vertical ||| horizontal ||| tabs ||| full ||| grid
    tabsFirstLayout = tabs ||| vertical ||| horizontal ||| full ||| grid

configure sysState conf = conf {layoutHook = hook (SystemState.hostName sysState)}
