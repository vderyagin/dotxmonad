module Actions ( goToBrowser
               , goToSlack
               ) where

import           XMonad
import           XMonad.Actions.WindowGo             (raiseNextMaybe)
import qualified XMonad.StackSet                     as W
import qualified Workspaces
import           XMonad.Actions.SpawnOn              (spawnHere)
import Queries (isBrowser)

defaultBrowser :: String
defaultBrowser = "brave-bin"

goToBrowser :: X ()
goToBrowser = raiseNextMaybe (windows (W.view . Workspaces.workspace $ 1) >> spawnHere defaultBrowser) isBrowser

goToSlack :: X ()
goToSlack = raiseNextMaybe (windows (W.view . Workspaces.workspace $ 5) >> spawnHere "slack") (className =? "Slack")
