module NamedScratchpads ( toggle
                        , hook) where

import           Queries                     (isImageViewer,
                                              isVideoPlayer)
import           XMonad
import           XMonad.Hooks.ManageHelpers  (doCenterFloat, doFullFloat)
import           XMonad.Util.NamedScratchpad hiding (hook)

scratchpads :: NamedScratchpads
scratchpads =
  [ NS "terminal"     termCmd    isTerm        doCenterFloat
  , NS "dev-terminal" devTermCmd isDevTerm     doFullFloat
  , NS "image-viewer" []         isImageViewer doCenterFloat
  , NS "video-player" []         isVideoPlayer doCenterFloat
  ]
  where
    isTerm, isDevTerm :: Query Bool
    isTerm    = className =? "Alacritty" <&&> title =? "sp_term"
    isDevTerm = className =? "Alacritty" <&&> title =? "sp_dev_term"

    termCmd, devTermCmd :: String
    termCmd    = "alacritty --title sp_term --option window.dimensions.columns=117 --option window.dimensions.lines=38 --command sh -c 'tmux attach -t main || tmux -q source-file ~/.config/tmux/main.conf \\; attach -t main'"
    devTermCmd = "alacritty --title sp_dev_term --command sh -c 'tmux attach -t dev || tmux -q source-file ~/.config/tmux/dev.conf \\; attach-session -t dev'"

hook :: ManageHook
hook = namedScratchpadManageHook scratchpads

toggle :: String -> X ()
toggle = namedScratchpadAction scratchpads
