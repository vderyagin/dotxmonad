{-# LANGUAGE NamedFieldPuns #-}

module SystemState (SystemState(..), getSystemState) where

import           System.Directory            (getHomeDirectory)
import           System.Posix.Unistd         (getSystemID, nodeName)
import           System.IO                   ()
import           XMonad.Util.Run             (runProcessWithInput)

data SystemState = SystemState { homeDir              :: FilePath
                               , hostName             :: String
                               , horizontalResolution :: Int
                               }

getSystemState :: IO SystemState
getSystemState = do
  homeDir <- getHomeDirectory
  hostName <- nodeName <$> getSystemID
  horizontalResolution <- read <$> runProcessWithInput "sh" ["-c", readResolutionCmd] ""
  return $ SystemState { homeDir, hostName, horizontalResolution }
    where readResolutionCmd = "xrandr | grep --after-context=1 'connected primary' | sed -r 's/^\\s+([0-9][0-9]*)x.+?\\*.*$/\\1/g;t;d'"
