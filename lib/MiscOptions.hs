{-# LANGUAGE KindSignatures      #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MiscOptions (configure) where

import           XMonad

configure :: forall (l :: * -> *). XConfig l -> XConfig l
configure c = c { borderWidth        = 1
                , clickJustFocuses   = False
                , focusFollowsMouse  = False
                , terminal           = "alacritty"
                }
