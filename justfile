export XMONAD_CONFIG_DIR := `pwd`
export XMONAD_CACHE_DIR := `echo ~/.xmonad`
export XMONAD_DATA_DIR := `echo ~/.xmonad`

default: && restart
    xmonad --recompile

restart:
    killall --signal SIGKILL dzen2
    xmonad --restart
